// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;
pragma abicoder v2;
import "./Ownable.sol";

contract Store is Ownable {
    // Buyers (clients) should be able to get the products length.
    // With this length the client can get product keys from productKeys array.
    // With these keys the client can get the quantities from products mapping
    uint256 public productsLentgth;
    string[] public productKeys;
    mapping(string => uint256) public products;
    mapping(address => mapping(string => uint256)) public availabilityByAddress;

    mapping(string => address[]) private dealsByProducts;
    mapping(address => mapping(string => bool)) private deals;
    mapping(address => mapping(string => uint256)) private dealsByBlockNumber;

    struct Product {
        string name;
        uint256 quantity;
    }

    event LogProductAdd(string productName, uint256 quantity);
    event LogProductUpdated(string productName, uint256 quantity);
    event LogProductBuy(string productName, uint256 quantity, address buyer);
    event LogProductReturn(string productName, uint256 quantity);

    // The clients should not be able to buy a product more times than the quantity in the store unless a product is returned or added by the administrator (owner)
    modifier productAvailability(string memory name) {
        require(productKeys.length > 0, "The store is empty!");
        require(products[name] > 0, "This product is not available in store!");
        _;
    }

    // A client cannot buy the same product more than one time.
    modifier canBuyProduct(Product memory product) {
        require(
            products[product.name] >= product.quantity,
            "There is not enough products in the store!"
        );
        require(
            availabilityByAddress[msg.sender][product.name] == 0 ||
                deals[msg.sender][product.name] == false,
            "The product has been already bought!"
        );
        _;
    }

    modifier canReturnProduct(Product memory product) {
        require(
            deals[msg.sender][product.name] == true,
            "You do not have this product!"
        );
        require(
            availabilityByAddress[msg.sender][product.name] >= product.quantity,
            "You are not able to return more than you have!"
        );
        require(
            block.number - dealsByBlockNumber[msg.sender][product.name] < 100,
            "The return period has expired!"
        );
        _;
    }

    // The administrator (owner) of the store should be able to add new products and the quantity of them.
    // The administrator should not be able to add the same product twice, just quantity.
    function addProduct(Product memory result) public onlyOwner {
        require(
            products[result.name] == 0,
            "This product is already available in store!"
        );
        products[result.name] = result.quantity;
        productKeys.push(result.name);
        productsLentgth += 1;
        emit LogProductAdd(result.name, result.quantity);
    }

    function updateProductQuantity(Product calldata result)
        public
        onlyOwner
        productAvailability(result.name)
    {
        products[result.name] = products[result.name] + result.quantity;
        emit LogProductUpdated(
            result.name,
            products[result.name] + result.quantity
        );
    }

    function buyProduct(Product memory product)
        public
        productAvailability(product.name)
        canBuyProduct(product)
    {
        address buyer = msg.sender;

        uint256 newProductQuantity = products[product.name] - product.quantity;
        products[product.name] = newProductQuantity;
        deals[buyer][product.name] = true;
        availabilityByAddress[buyer][product.name] = product.quantity;
        dealsByBlockNumber[buyer][product.name] = block.number;
        address[] storage buyers = dealsByProducts[product.name];
        buyers.push(buyer);
        emit LogProductBuy(product.name, product.quantity, buyer);
    }

    // Buyers should be able to return products if they are not satisfied (within a certain period in blocktime: 100 blocks).
    function returnProduct(Product memory product)
        public
        canReturnProduct(product)
    {
        uint256 newProductQuantity = products[product.name] + product.quantity;
        products[product.name] = newProductQuantity;

        availabilityByAddress[msg.sender][product.name] =
            availabilityByAddress[msg.sender][product.name] -
            product.quantity;
        emit LogProductReturn(product.name, product.quantity);
    }

    // Everyone should be able to see the addresses of all clients that have ever bought a given product. (getAddressesByProductName)
    // With them everyone can be able to see the available quantity for this address and product. (getQuantityByAddress)
    function getAddressesByProductName(string memory productName)
        public
        view
        returns (address[] memory)
    {
        return dealsByProducts[productName];
    }
}
